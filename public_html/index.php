<?PHP

/*
To call from another tool, use:
php /data/project/catscan2/public_html/quick_intersection_test.php  --cats 'Victoria and Albert Museum' --lang commons --project wikimedia --tool CALLING_TOOL_NAME --ns 6 --depth 5
*/

if ( PHP_SAPI === 'cli' ) {
	chdir ( '/data/project/quick-intersection/public_html' ) ;
} else {
	error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
	ini_set('display_errors', 'On');
}

include_once ( 'php/wikiquery.php' ) ;
require_once ( 'php/pagepile.php' ) ;



function indent($json) {

    $result      = '';
    $pos         = 0;
    $strLen      = strlen($json);
    $indentStr   = '  ';
    $newLine     = "\n";
    $prevChar    = '';
    $outOfQuotes = true;

    for ($i=0; $i<=$strLen; $i++) {

        // Grab the next character in the string.
        $char = substr($json, $i, 1);

        // Are we inside a quoted string?
        if ($char == '"' && $prevChar != '\\') {
            $outOfQuotes = !$outOfQuotes;

        // If this character is the end of an element,
        // output a new line and indent the next line.
        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
            $result .= $newLine;
            $pos --;
            for ($j=0; $j<$pos; $j++) {
                $result .= $indentStr;
            }
        }

        // Add the character to the result string.
        $result .= $char;

        // If the last character was the beginning of an element,
        // output a new line and indent the next line.
        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
            $result .= $newLine;
            if ($char == '{' || $char == '[') {
                $pos ++;
            }

            for ($j = 0; $j < $pos; $j++) {
                $result .= $indentStr;
            }
        }

        $prevChar = $char;
    }

    return $result;
}

$default_max = 30000 ;
$absolute_max = 300000 ;
$time0 = microtime ( true ) ;
$out = array() ;
$get_count = false ;
$group_by_key = false ;
$wikidata = false ;
$wikidata_no_item = false ;

if ( CLI ) {
	// Defaults
	$format = 'json' ;
	$start = 0 ;
	$max = $default_max ;
	$ns = 0 ;
	$depth = 0 ;
	$cats = '' ;
	$callback = '' ;
	$catlist = 0 ;
	$redirects = '' ;
	
	$giu = 0 ; // Default
	$sparse = 0 ; // Default
	$longopts = array ( 'cats:' , 'lang:' , 'project:' , 'tool:' , 'ns:' , 'depth:' , 'max:' , 'giu:' , 'sparse:' ) ;
	$shortopts = '' ;
	$tool = '' ;
	foreach ( $longopts AS $k => $v ) {
		$$v = '' ;
		$s = substr ( $v , 0 , 1 ) ;
		if ( substr ( $v , -2 , 2 ) == '::' ) $s .= '::' ;
		else if ( substr ( $v , -1 , 1 ) == ':' ) $s .= ':' ;
		$shortopts .= $s ;
	}
	$options = getopt($shortopts, $longopts);
	foreach ( $options AS $k => $v ) {
		$$k = $v ;
	}
	if ( $cats=='' or $lang=='' or $project=='' or $tool=='' ) {
		print "Needs --cats, --lang, --project\n" ;
		exit ( 0 ) ;
	}
	$tool_user_name = $tool ;

} else {
	$cats = get_request('cats','');
	$lang = get_request('lang','en');
	$project = get_request('project','wikipedia');
	$format = get_request('format','html');
	$start = get_request('start',0)*1;
	$max = get_request('max',$default_max)*1;
	$ns = get_request('ns',0);
	if ( $ns.'' != '*' ) $ns = 1 * $ns ;
	$redirects = get_request('redirects','');
	$depth = get_request('depth',12)*1;
	$callback = get_request('callback','');
	$sparse = isset ( $_REQUEST['sparse'] ) ;
	$catlist = isset ( $_REQUEST['catlist'] ) ;
	$group_by_key = isset ( $_REQUEST['group_by_key'] ) ;
	$get_count = isset ( $_REQUEST['get_count'] ) ;
	$giu = isset ( $_REQUEST['giu'] ) ;
	$wikidata = isset ( $_REQUEST['wikidata'] ) ;
	$wikidata_no_item = isset ( $_REQUEST['wikidata_no_item'] ) ;
}

if ( $giu ) {
	$sparse = false ;
	$ns = 6 ;
	$lang = 'commons' ;
	$project = 'wikimedia' ;
}

$pagepile_label = '' ;
if ( $pagepile_enabeled ) {
	$pagepile_label = "<label class='radio inline'><input type='radio' name='format' value='pagepile' ".($format=='pagepile'?'checked':'')."/>PagePile</label>" ;
}


if ( $cats=='' or isset ( $_REQUEST['norun'] ) ) {
	header('Content-type: text/html; charset=utf-8');
	print get_common_header('','Quick intersection');
	print "
<p>This tool utilizes the WMF Labs databases to generate very fast category trees and tree intersections.</p>
<div>
<form method='get' class='form form-inline'>
<table class='table table-condensed table-striped'>
<tr><th>Language</th><td><input type='text' name='lang' value='$lang' /> <span style='color:red'>(required)</span></th></td></tr>
<tr><th>Project</th><td><input type='text' name='project' value='$project' /> <span style='color:red'>(required)</span></th></td></tr>
<tr><th nowrap>Root categories</th><td style='width:100%'><textarea style='width:100%' rows=5 cols=50 name='cats'>$cats</textarea><br/>
<small>(One category per line) <span style='color:red'>(required)</span></small></td></tr>
<tr><th nowrap>Namespace</th><td><input type='text' name='ns' value='$ns' class='span2' /> (0=articles, 6=files, *=all)</td></tr>
<tr><th>Depth</th><td><input type='text' name='depth' value='$depth' class='span2' /> (-1=unlimited)</td></tr>
<tr><th>Max results</th><td><input type='text' name='max' value='$max' class='span2' /></td></tr>
<tr><th>Offset</th><td><input type='text' name='start' value='$start' class='span2' /></td></tr>
<tr><th>Format</th><td>
<label class='radio inline'><input type='radio' name='format' value='html' ".($format=='html'?'checked':'')." />HTML</label>
<label class='radio inline'><input type='radio' name='format' value='wiki' ".($format=='wiki'?'checked':'')."/>Wiki</label>
<label class='radio inline'><input type='radio' name='format' value='json' ".($format=='json'?'checked':'')."/>JSON</label>
<label class='radio inline'><input type='radio' name='format' value='jsonfm' ".($format=='jsonfm'?'checked':'')."/>pretty JSON</label>
$pagepile_label
</td></tr>
<tr><th nowrap>Image usage</th><td><label><input type='checkbox' name='giu' value='1' ".($giu?'checked':'')."/> Global image usage (enforces commons.wikimedia, namespace 6, not sparse)</label></td></tr>
<tr><th>Sparse</th><td><label><input type='checkbox' name='sparse' value='1' ".($sparse?'checked':'')."/> Just return namespace-prefixed page titles (JSON only)</label></td></tr>
<tr><th>Group</th><td><label><input type='checkbox' name='group_by_key' value='1' ".($group_by_key?'checked':'')."/> Group results by category key (JSON only, not for images)</label></td></tr>
<tr><th>Count</th><td><label><input type='checkbox' name='get_count' value='1' ".($get_count?'checked':'')."/> Get count of all possible results as well (JSON only; set 'Max results' to just get the count)</label></td></tr>
<tr><th>Category list</th><td><label><input type='checkbox' name='catlist' value='1' ".($catlist?'checked':'')."/> Get the categories containing the individual page</label></td></tr>

<tr><th>Redirects</th><td>
<label><input type='radio' name='redirects' value='' ".($redirects==''?'checked':'')."/> either</label>
<label><input type='radio' name='redirects' value='none' ".($redirects=='none'?'checked':'')."/> none</label>
<label><input type='radio' name='redirects' value='only' ".($redirects=='only'?'checked':'')."/> only</label>
</td></tr>

<tr><th>Wikidata item</th><td><label><input type='checkbox' name='wikidata' value='1' ".($wikidata?'checked':'')."/> Get the corresponding Wikidata item for the individual page</label> (<label><input type='checkbox' name='wikidata_no_item' value='1' ".($wikidata_no_item?'checked':'')."/> only pages without items</label>)</td></tr>
<tr><th nowrap>JSONP</th><td><input type='text' name='callback' value='$callback' /> Name of callback function (JSON only)</td></tr>
<tr><td/><td><input type='submit' class='btn btn-primary' value='Do it!' /> (add <tt>&norun</tt> to the URL to get this pre-filled form again)</td></tr>
</table>
</form>
</div>" ;
	print get_common_footer();
	exit ( 0 ) ;
}

if ( CLI ) ini_set("memory_limit","5G");
else if ( $max > $default_max ) {
	ini_set("memory_limit","2000M");
	if ( $max > $absolute_max ) $max = $absolute_max ;
} else ini_set("memory_limit","500M");

$cats = str_replace ( "\n" , "|" , $cats ) ;
$cats = str_replace ( ' ' , '_' , $cats ) ;
$cats = explode ( '|' , $cats ) ;
foreach ( $cats AS $k => $v ) $cats[$k] = trim ( $v ) ;

$db = openDB ( $lang , $project ) ;
if ( $lang == 'wikidata' ) $lang = 'www' ;

$subcats = array() ;
foreach ( $cats AS $c ) {
	$a = array() ;
	findSubcats ( $db , array($c) , $a , $depth ) ;
	$subcats[] = $a ;
}

$cond = array() ;
foreach ( $subcats AS $sc ) {
	$sc2 = array() ;
	foreach ( $sc AS $c => $d ) {
		$sc2[] = $db->escape_string ( $c ) ;
	}
	$co = "'" . join ( "','" , $sc2 ) . "'" ;
	$cond[] = $co ;
}


$out['namespaces'] = array() ;
$wq = new WikiQuery ( $lang , $project ) ;
$tmp = $wq->get_namespaces() ;
foreach ( $tmp AS $num => $name ) {
	$out['namespaces'][(string)$num] = $name ;
}


if ( $get_count ) {
	$sql = "select count(distinct p.page_id) AS cnt" ;
	$sql .= " from ( SELECT * from categorylinks where cl_to IN (" . $cond[0] ;
	$sql .= ")) cl0" ;
	foreach ( $cond AS $id => $c ) {
		if ( $id == 0 ) continue ;
		$sql .= " INNER JOIN categorylinks cl$id on cl0.cl_from=cl$id.cl_from and cl$id.cl_to IN ($c)" ;
	}
	$sql .= " INNER JOIN (page p) on p.page_id=cl0.cl_from" ;
	if ( $ns.'' != '*' ) $sql .= " and page_namespace=$ns" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($row = $result->fetch_assoc()){
		$out['total_results'] = $row['cnt'] ;
	}
}

$sql = "select distinct p.*" ;
if ( $giu ) $sql .= ",g.*" ;
if ( $catlist ) $sql .= ",group_concat(DISTINCT cl0.cl_to SEPARATOR '|') AS cats" ;
$sql .= ",cl0.cl_sortkey" ;
$sql .= " FROM ( SELECT * from categorylinks where cl_to IN (" . $cond[0] ;
$sql .= ")) cl0" ;
foreach ( $cond AS $id => $c ) {
	if ( $id == 0 ) continue ;
	$sql .= " INNER JOIN categorylinks cl$id on cl0.cl_from=cl$id.cl_from and cl$id.cl_to IN ($c)" ;
}
$sql .= " INNER JOIN (page p" ;
if ( $giu ) $sql .= ",globalimagelinks g" ;
$sql .= ") on p.page_id=cl0.cl_from" ;
if ( $ns.'' != '*' ) $sql .= " and page_namespace=$ns" ;
if ( $giu ) $sql .= " AND gil_to=page_title" ;
if ( $redirects == 'only' ) $sql .= " AND page_is_redirect=1" ;
if ( $redirects == 'none' ) $sql .= " AND page_is_redirect=0" ;

//if ( $catlist ) 
$sql .= " GROUP BY p.page_id" ; // Could return multiple results per page in normal search, thus making this GROUP BY general
if ( $giu ) $sql .= ",gil_wiki,gil_page" ;

$sql .= " ORDER BY cl0.cl_sortkey" ;
if ( $max > 0 ) $sql .= " LIMIT $start,$max" ;

#header('Content-type: text/plain; charset=utf-8');
#print $sql ;
#$out['sql'] = $sql ;

if ( !$sparse ) $out['cats'] = $cats ;
$out['start'] = $start ;
$out['max'] = $max ;
$out['status'] = 'OK' ;
$out['querytime'] = '' ;
$out['pagecount'] = 0 ;
$out['pages'] = array() ;

if ( $max > 0 ) { # Probably trying to just get the total_count
//if ( CLI ) print "$sql\n" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($row = $result->fetch_assoc()){
		$row['cl_sortkey'] = utf8_encode ( $row['cl_sortkey'] ) ;
		$key = substr($row['cl_sortkey'],0,1) ;
		if ( $sparse ) {
			$t = $row['page_title'] ;
			if ( $row['page_namespace'] != 0 ) $t = $out['namespaces'][$row['page_namespace']] . ":" . $t ;
			if ( $group_by_key ) $out['pages'][$key][] = $t ;
			else $out['pages'][] = $t ;
		} else if ( $giu ) {
			if ( !isset ( $out['pages'][$row['page_id']] ) ) {
				$o = array ( 'giu' => array() ) ;
				foreach ( $row AS $k => $v ) {
					if ( substr ( $k , 0 , 5 ) != 'page_' ) continue ;
					$o[$k] = $v ;
				}
				$out['pages'][$row['page_id']] = $o ;
			}
			$out['pages'][$row['page_id']]['giu'][] = array ( 'wiki' => $row['gil_wiki'] , 'page' => $row['gil_page_title'] , 'ns' => $row['gil_page_namespace_id'] ) ;
		} else {
			if ( $group_by_key ) $out['pages'][$key][] = $row ;
			else $out['pages'][] = $row ;
		}
	}
}

$db->close();

if ( $wikidata ) {
	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	$title2id = array() ;
	$q = array() ;
	if ( $sparse ) {
		foreach ( $out['pages'] AS $k => $v ) {
			$title = str_replace ( '_' , ' ' , $v ) ;
			$title2id[$title] = $k ;
			$q[] = $db->escape_string ( $title ) ;
		}
	} else {
		foreach ( $out['pages'] AS $k => $v ) {
			$title = $v['page_title'] ;
			if ( $v['page_namespace'] != 0 ) $title = $out['namespaces'][$v->page_namespace] . '.' . $title ;
			$title = str_replace ( '_' , ' ' , $title ) ;
			$title2id[$title] = $k ;
			$q[] = $db->escape_string ( $title ) ;
		}
	}
	$site = $db->escape_string ( $lang ) ;
	if ( $project == 'wikipedia' ) $site .= 'wiki' ;
	else $site .= $db->escape_string ( $project ) ;
	$sql = "SELECT ips_site_page,ips_item_id FROM wb_items_per_site WHERE ips_site_id='$site' AND ips_site_page IN (\"" . implode ( '","' , $q ) . "\")" ;
//	$out['sql'] = $sql ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($r = $result->fetch_assoc()){
		$id = $title2id[$r['ips_site_page']] ;
		if ( isset ( $id ) ) {
			if ( $wikidata_no_item ) unset ( $out['pages'][$id] ) ;
			else $out['pages'][$id]['q'] = 'Q'.$r['ips_item_id'] ;
		}
	}
	$db->close() ;
}

$time1 = microtime ( true ) ;
$out['querytime'] = sprintf ( "%2.4fs" , ($time1 - $time0)/1000 ) ;
if ( $group_by_key ) {
	foreach ( $out['pages'] AS $p ) $out['pagecount'] += count ( $p ) ;
} else $out['pagecount'] = count ( $out['pages'] ) ;

// OUTPUT

if ( $format == 'html' and $sparse == 1 ) $format = 'jsonfm' ;

if ( !CLI ) $myurl = "http://" . $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"] ;
if ( $format == 'html' ) {
	header('Content-type: text/html; charset=utf-8');
	print get_common_header('','Category tree intersection');
	print "<p>Query took " . $out['querytime'] . "; " . $out['pagecount'] . " pages were found. " ;
	print "Link to this query <a href='$myurl'>with</a> and <a href='$myurl&norun'>without</a> running it automatically.</p>" ;
	print "<div><table class='table table-condensed table-striped'>" ;
	print "<thead><tr><th>Title</th><th>Namespace</th><th>Last change</th><th>Size</th><th>Page ID</th>" ;
	if ( $catlist ) print "<th>Categories</th>" ;
	if ( $wikidata ) print "<th>Wikidata</th>" ;
	print "</tr></thead><tbody>" ;
	foreach ( $out['pages'] AS $p ) {
		$t = $p['page_title'] ;
		$nsn = '' ;
		if ( $p['page_namespace'] != 0 ) $nsn = $out['namespaces'][$p['page_namespace']] ;
		print "<tr><td><a target='_blank' href='//$lang.$project.org/wiki/" . ($nsn==''?'':"$nsn:") . urlencode ( $t ) ;
		print "'>" . str_replace ( "_" , " " , $t ) . "</a></td>" ;
		print "<td>" . ($p['page_namespace']==0?"(Article)":str_replace('_',' ',$nsn)) . "</td>" ;
		print "<td>" . $p['page_touched'] . "</td>" ;
		print "<td>" . $p['page_len'] . "</td>" ;
		print "<td>" . $p['page_id'] . "</td>" ;
		if ( $catlist ) {
			$x = array() ;
			foreach ( explode('|',str_replace('_',' ',$p['cats'])) AS $y ) $x[] = "<a target='_blank' href='//$lang.$project.org/wiki/Category:".urlencode(str_replace(' ','_',$y))."'>$y</a>" ;
			print "<td>" . implode ( "<br/>" , $x ) . "</td>" ;
		}
		if ( $wikidata ) {
			print "<td>" ;
			if ( isset ( $p['q'] ) ) print "<a href='//www.wikidata.org/wiki/".$p['q']."' target='_blank'>".$p['q']."</a>" ;
			else print "&mdash;" ;
			print "</td>" ;
		}
		print "</tr>" ;
	}
	print "</tbody></table></div>" ;
	print get_common_footer();
//	print "</body></html>" ;

} else if ( $format == 'wiki' ) {
	header('Content-type: text/plain; charset=utf-8');
	print "== Category intersection ==\n" ;
	print "=== Query details ===\n" ;
	print "* Query was run on " . date('Y-m-d H:i:s') . "\n" ;
	print "* Query took " . $out['querytime'] . "\n" ;
	print "* Query yielded " . $out['pagecount'] . " pages\n" ;
	print "* [$myurl Rerun query]\n" ;
	print "* [$myurl&norun Open query without running it]\n" ;
	print "=== Root categories ===\n" ;
	foreach ( $cats AS $c ) {
		$c = str_replace ( '_' , ' ' , $c ) ;
		print "* [[:Category:$c|$c]]\n" ;
	}
	print "=== Results ===\n" ;
	print "{|class='wikitable sortable'\n!Title!!Namespace!!Last change!!Size!!Page ID" ;
	if ( $catlist ) print "!!Categories" ;
	if ( $wikidata ) print "!!Wikidata" ;
	print "\n" ;
	foreach ( $out['pages'] AS $p ) {
		$t = str_replace ( '_' , ' ' , $p['page_title'] ) ;
		$nsn = '' ;
		if ( $p['page_namespace'] != 0 ) $nsn = $out['namespaces'][$p['page_namespace']] ;
		print "|-\n" ;
		print "|[[" . ($nsn==''?'':"$nsn:") . "$t|$t]]" ;
		print " || " . ($p['page_namespace']==0?"(Article)":str_replace('_',' ',$nsn)) ;
		print " || " . $p['page_touched'] ;
		print " || " . $p['page_len'] ;
		print " || " . $p['page_id'] ;
		if ( $catlist ) print " || [[:Category:" . implode("|]]<br/>[[:Category:",explode('|',str_replace('_',' ',$p['cats']))) . "|]]" ;
		if ( $wikidata ) print " || " . (isset($p['q'])?'[[d:'.$p['q'].'|]]':'') ;
		print "\n" ;
	}
	print "|}\n" ;

} else if ( $format == 'pagepile' ) {
	
	$pp = new PagePile ;
	$pp->createNewPile ( $lang , $project ) ;
	$pp->beginTransaction() ;
	
	if ( $sparse ) {
	
		// TODO
	
	} else {
		
		foreach ( $out['pages'] AS $p ) {
			$pp->addPage ( $p['page_title'] , $p['page_namespace'] ) ;
		}
		
	}
	
	$pp->commitTransaction() ;
	$pp->printAndEnd() ;
	

} else if ( $format == 'printr' ) {
	header('Content-type: text/plain; charset=utf-8');
	print_r ( $out ) ;
} else if ( $format == 'jsonfm' ) {
	header('Content-type: text/html; charset=utf-8');
	print "<p>This is a prettyfied JSON display. It is not for machine consumption; use '&format=json' for that.</p><hr/>" ;
	print "<pre>" ;
	if ( $callback != '' ) print $_REQUEST['callback'] . "(" ;
	print indent ( json_encode ( $out ) ) ;
	if ( $callback != '' ) print ")" ;
	print "</pre>" ;

} else { // Default : JSON
	if ( !CLI ) header('Content-type: application/json; charset=utf-8');
	if ( $callback != '' ) print $_REQUEST['callback'] . "(" ;
	print json_encode ( $out ) ;
	if ( $callback != '' ) print ")" ;
}

?>